#!/bin/sh

#Auteur : Corentin MULLER
#Date : 10/01/2020
#Version : 1.0.0 : Met en oeuvre un serveur virtuel (FREEBSD). (le site par defaut est operationel)

#Saisie utilisateur (Aucune fonctionnalite de controle saisie) : s'assurer de la coherence des informations renseignees
echo "Attention : Apache doit deja etre installe sur le serveur."
echo "Entrez le nom complet de votre serveur (svr-web.exemple.fr) : "
read servername
echo "Entrez le nom complet de votre virtualhost (site1.exemple.fr) : "
read vhostname
echo "Entrez le port d'ecoute (80/8080) :"
read vhostport

#Creation d'un fichier de configuration pour le serveur virtuel
cd /usr/local/etc/apache24/Includes
touch $vhostname.conf
echo "
Listen $vhostport
<VirtualHost *:$vhostport>
DocumentRoot \"/usr/local/www/$vhostname\"
<Directory \"/usr/local/www/$vhostname\">
	Require all granted
</Directory>
ServerName $vhostname
ErrorLog \"/var/log/$vhostname/$vhostname-error.log\"
CustomLog \"/var/log/$vhostname/$vhostname-access.log\" common
</VirtualHost>

<VirtualHost *:80>
ServerName $servername
</VirtualHost>" > $vhostname.conf

#Creation des fichiers de log
cd /var/log/
mkdir $vhostname
touch $vhostname-error.log
touch $vhostname-access.log 

#Creation d'un ficher html pour tests
cd /usr/local/www/
mkdir $vhostname
cd /usr/local/www/$vhostname
touch index.html
echo "<!DOCTYPE html>
<html>
<head>
	<title>Test $vhostname</title>
</head>
<body>
	<p>Page de test du serveur virtuel $vhostname</p>
</body>
</html>" > index.html

#Redemarrage d'apache 
service apache24 restart

#Message post execution
echo "
 - Racine web du serveur virtuel : /usr/local/www/$vhostname
 - Log d'erreur du serveur virtuel : /var/log/$vhostname/$vhostname-error.log
 - Log d'acces du serveur virtuel : /var/log/$vhostname/$vhostname-access.log

Dans le DNS, pensez a rajouter un enregistrement de type CNAME ($vhostname) se rapportant a l'adresse IP du serveur web. 
- editer le fichier /etc/hosts  (192.168.1.3    BSD14   IP = $vhostname)
- editer le fichier httpd.conf (/usr/local/etc/apache24/httpd.conf) -> ServerName BSD14:80
"







