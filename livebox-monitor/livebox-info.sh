#!/bin/bash

LIVEBOX_IP="192.168.1.1"
USERNAME="admin"
PASSWORD="Your admin password"
COOKIE_SAVE_PATH="/root/"


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

LOGIN_RESPONSE=`curl 'http://'"${LIVEBOX_IP}"'/ws' -s -X POST -H 'User-Agent: \
Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' \
-H 'Accept: */*' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H \
'Accept-Encoding: gzip, deflate' -H 'Authorization: X-Sah-Login' -H 'Content-T\
ype: application/x-sah-ws-4-call+json' -H 'Origin: http://'"${LIVEBOX_IP}" -H 'DNT\
: 1' -H 'Connection: keep-alive' -H 'Referer: http://'"${LIVEBOX_IP}"'/' -H 'C\
ookie: 3e673c0f/accept-language=fr,fr-FR; UILang=fr' --data-raw '{"service":"sa\
h.Device.Information","method":"createContext","parameters":{"applicationName":\
"webui","username":"'"${USERNAME}"'","password":"'"${PASSWORD}"'"}}' -c "${COOK\
IE_SAVE_PATH}"'.cookie_livebox'`

regex_pattern='contextID\"\:\"([^\"]+)'

[[ $LOGIN_RESPONSE =~ $regex_pattern ]]

CONTEXT_ID=${BASH_REMATCH[1]}

GET_RESPONSE=`curl 'http://'"${LIVEBOX_IP}"'/ws' -s -X POST -H 'User-Agent: \
Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -\
H 'Accept: */*' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'A\
ccept-Encoding: gzip, deflate' -H 'Content-Type: application/x-sah-ws-4-call+js\
on' -H 'Authorization: X-Sah '"${CONTEXT_ID}"'' -H 'X-Context: '"${CONTEXT_ID}"\
'' -H 'Origin: http://'"${LIVEBOX_IP}" -H 'DNT: 1' -H 'Connection: keep-alive' -H '\
Referer: http://'"${LIVEBOX_IP}"'/' -b "${COOKIE_SAVE_PATH}"'.cookie_livebox' -\
-data-raw '{"service": "NeMo.Intf.veip0", "method": "getMIBs", "parameters": {"mibs":"gpon"}}'`

SIGNAL_RX_POWER=$(echo $GET_RESPONSE | jq -r | grep "SignalRxPower" | awk -F ': ' '{print $2}' | awk -F ',' '{print $1}')
SIGNAL_TX_POWER=$(echo $GET_RESPONSE | jq -r | grep "SignalTxPower" | awk -F ': ' '{print $2}' | awk -F ',' '{print $1}')
TEMP=$(echo $GET_RESPONSE | jq -r | grep "Temperature" | awk -F ': ' '{print $2}' | awk -F ',' '{print $1}')
VOLTAGE=$(echo $GET_RESPONSE | jq -r | grep "Voltage" | awk -F ': ' '{print $2}' | awk -F ',' '{print $1}')
DOWNLOAD=$(echo $GET_RESPONSE | jq -r | grep "DownstreamCurrRate" | awk -F ': ' '{print $2}' | awk -F ',' '{print $1}')
UPLOAD=$(echo $GET_RESPONSE | jq -r | grep "UpstreamCurrRate" | awk -F ': ' '{print $2}' | awk -F ',' '{print $1}')


GET_WAN_STAT=`curl 'http://'"${LIVEBOX_IP}"'/ws' -s -X POST -H 'User-Agent: \
Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -\
H 'Accept: */*' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'A\
ccept-Encoding: gzip, deflate' -H 'Content-Type: application/x-sah-ws-4-call+js\
on' -H 'Authorization: X-Sah '"${CONTEXT_ID}"'' -H 'X-Context: '"${CONTEXT_ID}"\
'' -H 'Origin: http://'"${LIVEBOX_IP}" -H 'DNT: 1' -H 'Connection: keep-alive' -H '\
Referer: http://'"${LIVEBOX_IP}"'/' -b "${COOKIE_SAVE_PATH}"'.cookie_livebox' -\
-data-raw '{"service": "HomeLan", "method": "getWANCounters", "parameters": {}}'`

RX_BYTES=$(echo $GET_WAN_STAT | jq -r | grep "BytesReceived" | awk -F ': ' '{print $2}' | awk -F ',' '{print $1}')
TX_BYTES=$(echo $GET_WAN_STAT | jq -r | grep "BytesSent" | awk -F ': ' '{print $2}' | awk -F ',' '{print $1}')




#echo $SIGNAL_RX_POWER
#echo $SIGNAL_TX_POWER
#echo $TEMP
#echo $VOLTAGE
#echo $DOWNLOAD
#echo $UPLOAD

# Calcul de mise a l'echelle des signaux en dbm

SIGNAL_RX_POWER_CALC=$(echo "$SIGNAL_RX_POWER * 0.001" | bc)
SIGNAL_TX_POWER_CALC=$(echo "$SIGNAL_TX_POWER * 0.001" | bc)


PREV_VALUES_FILE="/root/network_stats.txt"

if [ -f $PREV_VALUES_FILE ]; then
    # Lire les valeurs précédentes
    PREV_RX_BYTES=$(awk '{print $1}' $PREV_VALUES_FILE)
    PREV_TX_BYTES=$(awk '{print $2}' $PREV_VALUES_FILE)

    # Calculer la différence en bytes
    DIFF_RX_BYTES=$((RX_BYTES - PREV_RX_BYTES))
    DIFF_TX_BYTES=$((TX_BYTES - PREV_TX_BYTES))

    # Convertir les bytes en bits (1 byte = 8 bits)
    DIFF_RX_BITS=$((DIFF_RX_BYTES * 8))
    DIFF_TX_BITS=$((DIFF_TX_BYTES * 8))

    # Convertir les bits en mégabits (1 mégabit = 10^6 bits)
    DIFF_RX_MBITS=$(echo "scale=2; $DIFF_RX_BITS / 1000000" | bc)
    DIFF_TX_MBITS=$(echo "scale=2; $DIFF_TX_BITS / 1000000" | bc)

    # Diviser par le nombre de secondes écoulées (300 secondes pour 5 minutes)
    RX_MBPS=$(echo "scale=2; $DIFF_RX_MBITS / 300" | bc)
    TX_MBPS=$(echo "scale=2; $DIFF_TX_MBITS / 300" | bc)


    # Rendre les valeurs positives
make_positive() {
    local value=$1

    # Vérifier si la valeur est négative
    if [ $(echo "$value < 0" | bc -l) -eq 1 ]; then
        # Rendre la valeur positive
        value=$(echo "-$value" | bc -l)
    fi

    echo $value
}


   positive_RX=$(make_positive $RX_MBPS)
   positive_TX=$(make_positive $TX_MBPS)


else
    # Si le fichier n'existe pas, créer un fichier avec les valeurs actuelles
    echo "$RX_BYTES $TX_BYTES" > $PREV_VALUES_FILE
fi

# Mettre à jour le fichier avec les valeurs actuelles
echo "$RX_BYTES $TX_BYTES" > $PREV_VALUES_FILE


curl --request POST \
"http://YOUR_INFLUXDB_SERVER_IP:8086/api/v2/write?org=YOUR_ORG&bucket=YOUR_BUKET&precision=ns" \
  --header "Authorization: Token YOUR_INFLUXDB_TOKEN" \
  --header "Content-Type: text/plain; charset=utf-8" \
  --header "Accept: application/json" \
  --data-binary '
    gpon,property_id=signal_rx_power data='"$SIGNAL_RX_POWER_CALC"'
    gpon,property_id=signal_tx_power data='"$SIGNAL_TX_POWER_CALC"'
    gpon,property_id=temp_ont data='"$TEMP"'
    gpon,property_id=voltage data='"$VOLTAGE"'
    gpon,property_id=download data='"$positive_RX"'
    gpon,property_id=upload data='"$positive_TX"'
    '


rm "${COOKIE_SAVE_PATH}"'.cookie_livebox'