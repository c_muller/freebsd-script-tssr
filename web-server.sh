#!/bin/sh

#Auteur : Corentin MULLER
#Date : 29/05/2022
#Version : 2.0.0 : Met en oeuvre un serveur Apache/MySql avec PHP (Freebsd Apache Mysql Php : FAMP)
# Base PHP 8.1

#Installation du service serveur web apache 
pkg install -y apache24

#Service apache démarré au boot 
sysrc apache24_enable=YES

# Installation de php 8.1
 pkg install -y php81

#Installation du mod_php (interaction php / apache) (package)
pkg install -y mod_php81


#Copie du fichier php.ini 
cd /usr/local/etc
cp php.ini-production php.ini

#Creation d'un fichier de configuration pour php
cd /usr/local/etc/apache24/Includes
touch php.conf
echo "
<FilesMatch \"\.php$\">
	SetHandler application/x-httpd-php
</FilesMatch>
<FilesMatch \"\.phps$\">
	SetHandler application/x-httpd-php-source
</FilesMatch>" > php.conf


#Installation de mysql server 8.0
pkg install -y mysql80-server

#Service mysql server demarre au boot 
sysrc mysql_enable=YES

#Demarrage du service mysql
service mysql-server start 

#Assistant de configuration mysql 
mysql_secure_installation

#Redemarrage du service mysql
service mysql-server restart

# Installation phpmyadmin (alias web)
pkg install -y phpMyAdmin5-php81

# Configuration Apache pour phpmyadmin
cd /usr/local/etc/apache24/Includes
touch phpmyadmin.conf
echo "Alias /phpmyadmin \"/usr/local/www/phpMyAdmin/\"
<Directory \"/usr/local/www/phpMyAdmin/\">
   Options None
   AllowOverride Limit
   Require all granted
   #Require host .example.com
</Directory>" > phpmyadmin.conf

#fichier de test php (phpinfo)
cd /usr/local/www/apache24/data
echo "
<?php
  phpinfo(); 
?>
">index.php

#Renomage de l'ancien fichier index.html
mv index.html index-old.html 

#Demarrage du service apache
service apache24 start

#message post installation 
echo "
- Adresse du serveur : http://<ip>/
- Adresse PHPMyAdmin : http://<ip>/phpmyadmin
- Definir la timezone pour php dans le fichier /usr/local/etc/php.ini (date.timezone = Europe/Paris)
- Ajouter index.php dans la configuration Apache /usr/local/etc/apache24/httpd.conf
 
<IfModule dir_module> 
	DirectoryIndex index.html index.php
</IfModule>"
