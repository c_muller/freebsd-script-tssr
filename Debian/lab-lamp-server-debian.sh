#!/bin/bash
#Auteur : Corentin MULLER
#Date : 24/05/2023
#Version : 1.0.0 : Met en oeuvre un serveur LAMP sous debian 11 (LAB)

# Dossier qui contient les fichiers de configuration des sites disponibles : /etc/apache2/sites-available/
# Dossier qui contient les fichiers de configuration (via un lien symbolique), des sites actifs : /etc/apache2/sites-enabled
# Creation d'un lien symbolique : ln -s /etc/apache2/sites-available/votre-configuration.conf /etc/apache2/sites-enabled/votre-configuration.conf
# ou a2enconf votre-configuration.conf
# ou a2ensite configuration-site.conf
# ou a2enmod <votre module>
# Racine web : /var/www/html

# Mise à jour des dépots
apt-get update

# Installation paquet wget 
#apt-get install -y wget

# Installation du serveur web apache
apt-get install -y apache2

# Installation des utilitaires apache
apt-get install -y apache2-utils

# Ajout du processus du serveur web apache au démarrage
systemctl enable apache2

# Activation des modules apache
## Reecritue d'url 
a2enmod rewrite
## Gestion de la compression
sudo a2enmod deflate
## en-tête http
sudo a2enmod headers
# gestion des certificats ssl
sudo a2enmod ssl

# Installation de PHP
apt-get install -y php

# Installations des dépendances PHP
apt-get install -y php-pdo php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath php-mysql

# Installation serveur de bdd MariaDB
apt-get install -y mariadb-server

# Commande d'installation et de configuration de MariaDB
mariadb-secure-installation

# Création d'une page PHP de test
echo "<?php
phpinfo();
?>" > /var/www/html/phpinfo.php

# Téléchargement de et extraction de l'archive phpmyAdmin 
cd /tmp
wget https://files.phpmyadmin.net/phpMyAdmin/5.2.1/phpMyAdmin-5.2.1-all-languages.zip 
apt install -y unzip
unzip phpMyAdmin-5.2.1-all-languages.zip

# Installation de PhpMyadmin dans /usr/share/phpmyadmin
mv phpMyAdmin-5.2.1-all-languages /usr/share/phpmyadmin

mkdir -p /var/lib/phpmyadmin/tmp
chown -R www-data:www-data /var/lib/phpmyadmin/
#cp /usr/share/phpmyadmin/config.sample.inc.php /usr/share/phpmyadmin/config.inc.php

#Saisie du mot de passe root definie par l'utilisateur
echo "Utilisateur pour la console phpmyadmin" 
read $User
echo "Mot de passe pour la console phpmyadmin" 
read $Password

# Generation du secret pour l'auth par cookie
BLOWFISH=$(openssl rand -base64 32)

# Configuration de phpmyadmin
echo "<?php
/**
 * phpMyAdmin sample configuration, you can use it as base for
 * manual configuration. For easier setup you can use setup/
 *
 * All directives are explained in documentation in the doc/ folder
 * or at <https://docs.phpmyadmin.net/>.
 */

declare(strict_types=1);

/**
 * This is needed for cookie based authentication to encrypt the cookie.
 * Needs to be a 32-bytes long string of random bytes. See FAQ 2.10.
 */
\$cfg['blowfish_secret'] = '$BLOWFISH'; /* YOU MUST FILL IN THIS FOR COOKIE AUTH! */

/**
 * Servers configuration
 */
\$i = 0;

/**
 * First server
 */
\$i++;
/* Authentication type */
\$cfg['Servers'][\$i]['auth_type'] = 'cookie';
/* Server parameters */
\$cfg['Servers'][\$i]['host'] = 'localhost';
\$cfg['Servers'][\$i]['compress'] = false;
\$cfg['Servers'][\$i]['AllowNoPassword'] = false;

/**
 * phpMyAdmin configuration storage settings.
 */

/* User used to manipulate with storage */
// \$cfg['Servers'][\$i]['controlhost'] = '';
// \$cfg['Servers'][\$i]['controlport'] = '';
 \$cfg['Servers'][\$i]['controluser'] = '$User';
 \$cfg['Servers'][\$i]['controlpass'] = '$Password';

/* Storage database and tables */
 \$cfg['Servers'][\$i]['pmadb'] = 'phpmyadmin';
 \$cfg['Servers'][\$i]['bookmarktable'] = 'pma__bookmark';
 \$cfg['Servers'][\$i]['relation'] = 'pma__relation';
 \$cfg['Servers'][\$i]['table_info'] = 'pma__table_info';
 \$cfg['Servers'][\$i]['table_coords'] = 'pma__table_coords';
 \$cfg['Servers'][\$i]['pdf_pages'] = 'pma__pdf_pages';
 \$cfg['Servers'][\$i]['column_info'] = 'pma__column_info';
 \$cfg['Servers'][\$i]['history'] = 'pma__history';
 \$cfg['Servers'][\$i]['table_uiprefs'] = 'pma__table_uiprefs';
 \$cfg['Servers'][\$i]['tracking'] = 'pma__tracking';
 \$cfg['Servers'][\$i]['userconfig'] = 'pma__userconfig';
 \$cfg['Servers'][\$i]['recent'] = 'pma__recent';
 \$cfg['Servers'][\$i]['favorite'] = 'pma__favorite';
 \$cfg['Servers'][\$i]['users'] = 'pma__users';
 \$cfg['Servers'][\$i]['usergroups'] = 'pma__usergroups';
 \$cfg['Servers'][\$i]['navigationhiding'] = 'pma__navigationhiding';
 \$cfg['Servers'][\$i]['savedsearches'] = 'pma__savedsearches';
 \$cfg['Servers'][\$i]['central_columns'] = 'pma__central_columns';
 \$cfg['Servers'][\$i]['designer_settings'] = 'pma__designer_settings';
 \$cfg['Servers'][\$i]['export_templates'] = 'pma__export_templates';

/**
 * End of servers configuration
 */

/**
 * Directories for saving/loading files from server
 */
\$cfg['UploadDir'] = '';
\$cfg['SaveDir'] = '';
\$cfg['TempDir'] = '/var/lib/phpmyadmin/tmp';

/**
 * Whether to display icons or text or both icons and text in table row
 * action segment. Value can be either of 'icons', 'text' or 'both'.
 * default = 'both'
 */
//\$cfg['RowActionType'] = 'icons';

/**
 * Defines whether a user should be displayed a \"show all (records)\"
 * button in browse mode or not.
 * default = false
 */
//\$cfg['ShowAll'] = true;

/**
 * Number of rows displayed when browsing a result set. If the result
 * set contains more rows, \"Previous\" and \"Next\".
 * Possible values: 25, 50, 100, 250, 500
 * default = 25
 */
//\$cfg['MaxRows'] = 50;

/**
 * Disallow editing of binary fields
 * valid values are:
 *   false    allow editing
 *   'blob'   allow editing except for BLOB fields
 *   'noblob' disallow editing except for BLOB fields
 *   'all'    disallow editing
 * default = 'blob'
 */
//\$cfg['ProtectBinary'] = false;

/**
 * Default language to use, if not browser-defined or user-defined
 * (you find all languages in the locale folder)
 * uncomment the desired line:
 * default = 'en'
 */
//\$cfg['DefaultLang'] = 'en';
//\$cfg['DefaultLang'] = 'de';

/**
 * How many columns should be used for table display of a database?
 * (a value larger than 1 results in some information being hidden)
 * default = 1
 */
//\$cfg['PropertiesNumColumns'] = 2;

/**
 * Set to true if you want DB-based query history.If false, this utilizes
 * JS-routines to display query history (lost by window close)
 *
 * This requires configuration storage enabled, see above.
 * default = false
 */
//\$cfg['QueryHistoryDB'] = true;

/**
 * When using DB-based query history, how many entries should be kept?
 * default = 25
 */
//\$cfg['QueryHistoryMax'] = 100;

/**
 * Whether or not to query the user before sending the error report to
 * the phpMyAdmin team when a JavaScript error occurs
 *
 * Available options
 * ('ask' | 'always' | 'never')
 * default = 'ask'
 */
//\$cfg['SendErrorReports'] = 'always';

/**
 * 'URLQueryEncryption' defines whether phpMyAdmin will encrypt sensitive data from the URL query string.
 * 'URLQueryEncryptionSecretKey' is a 32 bytes long secret key used to encrypt/decrypt the URL query string.
 */
//\$cfg['URLQueryEncryption'] = true;
//\$cfg['URLQueryEncryptionSecretKey'] = '';

/**
 * You can find more configuration options in the documentation
 * in the doc/ folder or at <https://docs.phpmyadmin.net/>.
 */" > /usr/share/phpmyadmin/config.inc.php

 # Utilisation du script fournie pour créer les tables
 mysql -u root -p < /usr/share/phpmyadmin/sql/create_tables.sql

 # Creation d'un administrateur Phpmyadmin
 mysql -uroot -p -e "CREATE USER '$User'@'localhost' IDENTIFIED BY '$Password';"
 mysql -uroot -p -e "GRANT ALL PRIVILEGES ON *.* TO '$User'@'localhost' WITH GRANT OPTION;"
 mysql -uroot -p -e "FLUSH PRIVILEGES;"


# Configuration apache 
echo "Alias /phpmyadmin /usr/share/phpmyadmin

<Directory /usr/share/phpmyadmin>
  Options SymLinksIfOwnerMatch
  DirectoryIndex index.php

  <IfModule mod_php.c>
    <IfModule mod_mime.c>
      AddType application/x-httpd-php .php
    </IfModule>
    <FilesMatch \".+\.php\$\">
      SetHandler application/x-httpd-php
    </FilesMatch>

    php_value include_path .
    php_admin_value upload_tmp_dir /var/lib/phpmyadmin/tmp
    
    php_admin_value open_basedir /usr/share/phpmyadmin/:/etc/phpmyadmin/:/var/lib/phpmyadmin/:/usr/share/php/php-gettext/:/usr/share/php/php-php-gettext/:/usr/share/javascript/:/usr/share/php/tcpdf/:/usr/share/doc/phpmyadmin/:/usr/share/php/phpseclib/
    
    php_admin_value mbstring.func_overload 0
  </IfModule>

</Directory>

# Désactiver accès web sur certains dossiers
<Directory /usr/share/phpmyadmin/templates>
  Require all denied
</Directory>
<Directory /usr/share/phpmyadmin/libraries>
  Require all denied
</Directory>
<Directory /usr/share/phpmyadmin/setup/lib>
  Require all denied
</Directory>" > /etc/apache2/conf-available/phpmyadmin.conf

# Création du lien symbolique pour activer la configuration
ln -s /etc/apache2/conf-available/phpmyadmin.conf /etc/apache2/conf-enabled/phpmyadmin.conf
# Redémarrage apache
service apache2 restart
