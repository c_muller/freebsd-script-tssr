#!/bin/bash
# Auteur : Corentin MULLER
# Date : 04/04/2021
# Ce script met en oeuvre les service DHCP et DNS dynamique sur debian 10
# Configuration du nom d'hôte du serveur 
echo "ns0.lab.net" > /etc/hostname

# Installation du service serveur dhcp
apt install isc-dhcp-server -y
sleep 15
# Arrêt du service dhcp
/etc/init.d/isc-dhcp-server stop
sleep 2
# Edition de la configuration du lancenement du serveur DHCP
echo "# Defaults for isc-dhcp-server (sourced by /etc/init.d/isc-dhcp-server)

# Path to dhcpd's config file (default: /etc/dhcp/dhcpd.conf).
#DHCPDv4_CONF=/etc/dhcp/dhcpd.conf
#DHCPDv6_CONF=/etc/dhcp/dhcpd6.conf

# Path to dhcpd's PID file (default: /var/run/dhcpd.pid).
#DHCPDv4_PID=/var/run/dhcpd.pid
#DHCPDv6_PID=/var/run/dhcpd6.pid

# Additional options to start dhcpd with.
#       Don't use options -cf or -pf here; use DHCPD_CONF/ DHCPD_PID instead
#OPTIONS=\"\"

# On what interfaces should the DHCP server (dhcpd) serve DHCP requests?
#       Separate multiple interfaces with spaces, e.g. \"eth0 eth1\".
INTERFACESv4=\"enp0s3\"
#INTERFACESv6=\"\"
" > /etc/default/isc-dhcp-server

# Edition de la configuration du serveur DHCP. 
cd /etc/dhcp
cp dhcpd.conf dhcpd_old.conf 
echo "" > dhcpd.conf

echo "# Interfaces
INTERFACES=\"enp0s3\";
#Cle permettant la maj securisee de la zone dns lab.net
include \"/etc/dhcp/rndc.key\";

# Mode de mise a jour du dns
ddns-updates on;
ddns-update-style interim ;

# distribution parametre du suffixe dns
option domain-name \"lab.net.\";

# distribution du parametre du serveur dns
option domain-name-servers 10.10.0.253;

# Temps utilise par defaut pour la validite des adresse IP
default-lease-time 600;

# Temps maximum pour la validite des adresse IP (10h)
max-lease-time 7200;

# Ce serveur dhcp fera autorite sur le subnet 10.10.0.0/24.
authoritative;

# Mise en place des log
log-facility local7 ;

# LAN parametrage plage et options dhcp pour le LAN 10.10.0.0/24
subnet 10.10.0.0 netmask 255.255.255.0 {
    range 10.10.0.1 10.10.0.10;
    option subnet-mask 255.255.255.0;
    option broadcast-address 10.10.0.255;
    option routers 10.10.0.254;
}

# Specification des zone DNS (recherche directe / inversee)
zone lab.net. {
primary 127.0.0.1;
key rndc-key;
}

zone 0.10.10.in-addr.arpa. {
primary 127.0.0.1;
key rndc-key;
}
" > dhcpd.conf

# Installation du service DNS 
apt install bind9 -y
sleep 15
# Arret du service bind 
/etc/init.d/bind9 stop
sleep 2
# Edition du fichier de configuration de lancement du servie DNS 
echo "#
# run resolvconf?
RESOLVCONF=no

# startup options for the server
OPTIONS=\"-u bind -4\"" > /etc/default/bind9

# Edition de la configuration du serveur DNS 
cd /etc/bind
cp named.conf named-old.conf
echo "// Cle utilisee pour les mises a jour dynamiques du dns
include \"/etc/bind/rndc.key\";

//Forwarders (si le nom a resoudre est distant)
options {
  forwarders {8.8.8.8;};
  listen-on {127.0.0.1; 10.10.0.253 ;};
};

// Configuration zone directe
Zone \"lab.net.\" {
Type master ;
File \"/etc/bind/db.lab.net\" ;
Allow-update { key rndc-key ; 127.0.0.1 ; 10.10.0.253 ;} ;
} ;

// Zone reverse LAN 10.10.0.0 
Zone \"0.10.10.in-addr.arpa.\" {
Type master ;
File \"/etc/bind/db.lab.rev.100.0\" ;
Allow-update { key rndc-key ; 127.0.0.1 ; 10.10.0.253 ;} ;
};" > named.conf

# Edition du fichier de zone de recherche directe
touch db.lab.net
echo "\$ORIGIN .
\$TTL 10800      ; 3 hours
lab.net                 IN SOA  ns0.lab.net. admin.lab.net. (
                                2007010404 ; serial
                                3600       ; refresh (1 hour)
                                600        ; retry (10 minutes)
                                86400      ; expire (1 day)
                                600        ; minimum (10 minutes)
                                )
                        NS      ns0.lab.net.
\$ORIGIN lab.net.
dhcp1                   A       10.10.0.252
ns0                     A       10.10.0.253
www                     A       10.10.0.251" > db.lab.net

# Edition du fichier de zone de recherche inversee
touch db.lab.rev.100.0
echo "\$ORIGIN .
\$TTL 3600       ; 1 hour
0.10.10.in-addr.arpa    IN SOA  lab.net. admin.lab.net. (
                                3          ; serial
                                3600       ; refresh (1 hour)
                                600        ; retry (10 minutes)
                                604800     ; expire (1 week)
                                86400      ; minimum (1 day)
                                )
                        NS      ns0.lab.net.
\$ORIGIN 0.10.10.in-addr.arpa.
251                     PTR     www.lab.net.
252                     PTR     dhcp1.lab.net.
253                     PTR     ns0.lab.net." > db.lab.rev.100.0



# Configuration du du fichier resolv.conf du serveur 
echo "search lab.net." > /etc/resolv.conf

# Generation de la clé de chiffrement rndc 
/usr/sbin/rndc-confgen -a
sleep 2
cp rndc.key /etc/dhcp/rndc.key

# Configuration des permissions sur le fichier rndc.key
chown root:bind rndc.key
chown root:root /etc/dhcp/rndc.key
chmod 640 rndc.key
chmod 640 /etc/dhcp/rndc.key

# Configuration des permissions sur le dossier bind 
chown -R bind:bind /etc/bind

# Configuration du fichier interfaces 
echo "#Primary Interfaces
auto enp0s3 
iface enp0s3 inet static 
  address 10.10.0.253
  netmask 255.255.255.0
  broadcast 10.10.0.255" > /etc/network/interfaces

# Redemarrage du service reseau 
/etc/init.d/networking restart
sleep 5

# Demarrage du service DNS - Bind 
/etc/init.d/bind9 start 
sleep 15

# Demarrage du service dhcp - isc-dhcp-server
/etc/init.d/isc-dhcp-server start
sleep 15

# Commentaire -commande diag 
echo "
Autoriser la creation des fichier jnl (apparmor)
Editer le fichier :  /etc/apparmor.d/usr.sbin.named
Modifier la ligne : /etc/bind/**r  -> /etc/bind/**rw
Redemarrer le service apparmor : /etc/init.d/apparmor restart

 Controler la presence des fichiers jnl 
 - ls /etc/bind | grep jnl

Commandes pour le diagnostique
 - tail -f /var/log/syslog  | grep jnl
 - tail -f /var/log/syslog  | grep dhcpd"
