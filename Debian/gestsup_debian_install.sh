#!/bin/bash
#Auteur : Corentin MULLER
#Date : 23/12/2023
#Version : 2.0 : Met en oeuvre un serveur LAMP sous debian 12 (lab) & installation de l'application gestsup
# Ajout https avec AC


# Dossier qui contient les fichiers des modules disponibles : /etc/apache2/mods-available/
# Dossier qui contient les fichiers des modules activés (via un lien symbolique), des sites actifs : /etc/apache2/mods-enabled
# Dossier qui contient les fichiers de configuration des sites disponibles : /etc/apache2/sites-available/
# Dossier qui contient les fichiers de configuration des sites activés (via un lien symbolique), des sites actifs : /etc/apache2/sites-enabled
# Creation d'un lien symbolique : ln -s /etc/apache2/sites-available/votre-configuration.conf /etc/apache2/sites-enabled/votre-configuration.conf
# ou a2enconf votre-configuration.conf
# ou a2ensite configuration-site.conf
# Racine web : /var/www/html

# Mise à jour des dépots
apt-get update

# Installation paquet wget 
apt-get install -y wget

# Installation paquet unzip
apt-get install -y unzip

# Installation du serveur web apache
apt-get install -y apache2

# Installation des utilitaires apache
apt-get install -y apache2-utils

# Ajout du processus du serveur web apache au démarrage
systemctl enable apache2

# Activation des modules apache
## Reecritue d'url 
a2enmod rewrite
## Gestion de la compression
sudo a2enmod deflate
## en-tête http
sudo a2enmod headers

# Installation de PHP
apt-get install -y php

# Installations des dépendances PHP
apt install php-{common,curl,gd,imap,intl,ldap,mbstring,mysql,xml,zip} -y

# Installation serveur de bdd MariaDB
apt-get install -y mariadb-server

# Commande d'installation et de configuration de MariaDB
mariadb-secure-installation

# Création d'une page PHP de test
echo "<?php
phpinfo();
?>" > /var/www/html/phpinfo.php

# Création du répertoire gestsup dans la racine web
cd /var/www/html
mkdir gestsup
cd /var/www/html/gestsup

# Téléchargement de l'archive gestsup (v 3.2.40)
wget https://gestsup.fr/downloads/versions/current/version/gestsup_3.2.40.zip
unzip gestsup_3.2.40.zip

# Changement du propriétaire sur le répertoire gestsup
chown -R www-data:www-data /var/www/html/


# Creation de la base de donnees gestsup
mysql -uroot -p -e "CREATE DATABASE gestsup;"

# Création de l'utilisateur gestsup-user
mysql -uroot -p -e "CREATE USER 'gestsup-user'@'localhost' IDENTIFIED BY 'gestsup-user';"
mysql -uroot -p -e "GRANT ALL ON gestsup.* TO 'gestsup-user'@'localhost';"
mysql -uroot -p -e "FLUSH PRIVILEGES;"


# Configuration du vhost
echo "fqdn du site ex : gestsup.lab.net"
read fqdnSite

# Création du répertoire pourtant le nom de valeur contenu dans la variable $fdnSite et le rpéertoire ca
cd /etc/ssl/
mkdir $fqdnSite
mkdir ca

# Accès en https
# OpenSSL : Création de la clé de l'autorité de certification
openssl genrsa -out /etc/ssl/ca/ca-key.pem 4096

# Création du certificat de l'autorité de certification (5 ans de validité)
openssl req -new -x509 -sha256 -days 1825 -key /etc/ssl/ca/ca-key.pem -out /etc/ssl/ca/ca-cert.pem

# Génération d'un certificat + clé privée
# La clé privée
openssl genrsa -out /etc/ssl/$fqdnSite/privKey.pem 4096

# La demande de signature du certificat
openssl req -new -sha256 -subj "/CN=lab.net" -key /etc/ssl/$fqdnSite/privKey.pem -out /etc/ssl/$fqdnSite/sign-request.csr

# Création d'un fichier de configuration pour ajout de la donné Subject Alternative Name

echo "subjectAltName=DNS:$fqdnSite" > /etc/ssl/$fqdnSite/san.cnf

# Création du certificat 
openssl x509 -req -sha256 -days 365 -in /etc/ssl/$fqdnSite/sign-request.csr -CA /etc/ssl/ca/ca-cert.pem -CAkey /etc/ssl/ca/ca-key.pem -out /etc/ssl/$fqdnSite/cert.pem -extfile /etc/ssl/$fqdnSite/san.cnf -CAcreateserial


# Activation du module ssl apache
a2enmod ssl

# Fichier de configuration gestsup.conf
echo "<VirtualHost *:443>
        ServerName $fqdnSite
        DocumentRoot /var/www/html/gestsup
        SSLEngine on
        SSLCertificateFile /etc/ssl/$fqdnSite/cert.pem
        SSLCertificateKeyFile /etc/ssl/$fqdnSite/privKey.pem
    </VirtualHost>" > /etc/apache2/conf-available/gestsup.conf 

# Création du lien symbolique pour activer la configuration
#ln -s /etc/apache2/conf-available/gestsup.conf /etc/apache2/conf-enabled/gestsup.conf
# Activation de la configuration (remplace le lien symbolique)
a2enconf gestsup.conf

# Redémarrage apache
service apache2 restart

# Message de post Installation 
echo "Après l'installation de gestsup, supprimez le répertoire install situé dans /var/www/gestsup/"
