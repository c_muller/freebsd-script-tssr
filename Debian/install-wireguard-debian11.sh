#!/bin/bash
#Auteur : Corentin MULLER
#Date : 15/12/2022
#Version : 1.0.0 : Met en oeuvre un serveur Wireguard sous debian 11

# mise à jour du système
apt-get update

# Installation du package wiregard
apt-get -y install wireguard

# Génération de la clé privée et publique du serveur (/etc/wireguard)
wg genkey | tee /etc/wireguard/wg-private.key | wg pubkey | tee /etc/wireguard/wg-public.key

# Récupération de la clé privée en variable
PRIVATE=$(cat /etc/wireguard/wg-private.key)

# Récupération de la clé publique en variable
PUBLIC=$(cat /etc/wireguard/wg-public.key)

# Création du fichier de configuration 
touch /etc/wireguard/wg0.conf

# Spécification de la configuration
echo "
[Interface]
Address = 192.168.3.1/24
SaveConfig = true
ListenPort = 51820
PrivateKey = $PRIVATE
" > /etc/wireguard/wg0.conf

# Activation de l'interface wg0
wg-quick up wg0

# Activation de l'interface wg0 au démarrage du système 
systemctl enable wg-quick@wg0.service


# Activation IP-Forwarding
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf

# installation du paquet UFW
apt install -y ufw

# Autorisation du service SSH
/usr/sbin/ufw allow 22/tcp

# Autorisation Wireguard
/usr/sbin/ufw allow 51820/udp

# Récupération du nom de l'interface en variable
INTNAME=$(ip link show | grep enp | awk '{print $2}' | tr -d \:)

# Edition du fichier before.rules
echo "
#
# rules.before
#
# Rules that should be run before the ufw command line added rules. Custom
# rules should be added to one of these chains:
#   ufw-before-input
#   ufw-before-output
#   ufw-before-forward
#

# Don't delete these required lines, otherwise there will be errors
*filter
:ufw-before-input - [0:0]
:ufw-before-output - [0:0]
:ufw-before-forward - [0:0]
:ufw-not-local - [0:0]
# End required lines


# allow all on loopback
-A ufw-before-input -i lo -j ACCEPT
-A ufw-before-output -o lo -j ACCEPT

# quickly process packets for which we already have a connection
-A ufw-before-input -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A ufw-before-output -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A ufw-before-forward -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

# drop INVALID packets (logs these in loglevel medium and higher)
-A ufw-before-input -m conntrack --ctstate INVALID -j ufw-logging-deny
-A ufw-before-input -m conntrack --ctstate INVALID -j DROP

# ok icmp codes for INPUT
-A ufw-before-input -p icmp --icmp-type destination-unreachable -j ACCEPT
-A ufw-before-input -p icmp --icmp-type time-exceeded -j ACCEPT
-A ufw-before-input -p icmp --icmp-type parameter-problem -j ACCEPT
-A ufw-before-input -p icmp --icmp-type echo-request -j ACCEPT

# ok icmp code for FORWARD
-A ufw-before-forward -p icmp --icmp-type destination-unreachable -j ACCEPT
-A ufw-before-forward -p icmp --icmp-type time-exceeded -j ACCEPT
-A ufw-before-forward -p icmp --icmp-type parameter-problem -j ACCEPT
-A ufw-before-forward -p icmp --icmp-type echo-request -j ACCEPT


# autoriser le forwarding pour le réseau distant de confiance (+ le réseau du VPN)
# Réseau VPN
-A ufw-before-forward -s 192.168.3.0/24 -j ACCEPT
-A ufw-before-forward -d 192.168.3.0/24 -j ACCEPT
# Accès au réseaux locaux
-A ufw-before-forward -s 192.168.1.0/24 -j ACCEPT
-A ufw-before-forward -d 192.168.1.0/24 -j ACCEPT


# allow dhcp client to work
-A ufw-before-input -p udp --sport 67 --dport 68 -j ACCEPT

#
# ufw-not-local
#
-A ufw-before-input -j ufw-not-local

# if LOCAL, RETURN
-A ufw-not-local -m addrtype --dst-type LOCAL -j RETURN

# if MULTICAST, RETURN
-A ufw-not-local -m addrtype --dst-type MULTICAST -j RETURN

# if BROADCAST, RETURN
-A ufw-not-local -m addrtype --dst-type BROADCAST -j RETURN

# all other non-local packets are dropped
-A ufw-not-local -m limit --limit 3/min --limit-burst 10 -j ufw-logging-deny
-A ufw-not-local -j DROP

# allow MULTICAST mDNS for service discovery (be sure the MULTICAST line above
# is uncommented)
-A ufw-before-input -p udp -d 224.0.0.251 --dport 5353 -j ACCEPT

# allow MULTICAST UPnP for service discovery (be sure the MULTICAST line above
# is uncommented)
-A ufw-before-input -p udp -d 239.255.255.250 --dport 1900 -j ACCEPT

# NAT - IP masquerade
*nat
:POSTROUTING ACCEPT [0:0]
-A POSTROUTING -o $INTNAME -j MASQUERADE

# don't delete the 'COMMIT' line or these rules won't be processed
COMMIT
" > /etc/ufw/before.rules

# Activation du service UFW au démarrage du serveur
/usr/sbin/ufw enable

# Redémarrage du service UFW
systemctl restart ufw

# Affichage interface Wireguard
wg show wg0

# Exemple configuration cliente
echo "

Exemple de configuration cliente :

[Interface]
PrivateKey = <la clé privée du PC>
Address = 192.168.3.2/24
[Peer]
PublicKey = $PUBLIC
AllowedIPs = 192.168.3.0/24, 192.168.1.0/24
Endpoint = <ip-serveur-debian>:51820


Ajouter un peer via cette commande : 

wg set wg0 peer <client_pubkey> allowed-ips 192.168.3.x/32
"
