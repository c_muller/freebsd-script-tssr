#!/bin/sh
#Auteur : Corentin MULLER
#Date : 27/12/2022
#Version : 1.0 : Met en oeuvre un serveur Apache WebDav

#Installation du service serveur web apache 
pkg install -y apache24

#Service apache démarré au boot 
sysrc apache24_enable=YES

# Configuration authentification digest + webdav

# Activation du module auth digest apache
#cd /usr/local/etc/apache24/
#sed -i '/^LoadModule auth_digest_module/s/^#//' httpd.conf
#LoadModule dav_module libexec/apache24/mod_dav.so
#LoadModule auth_digest_module libexec/apache24/mod_auth_digest.so
#LoadModule dav_fs_module libexec/apache24/mod_dav_fs.so
#LoadModule dav_lock_module libexec/apache24/mod_dav_lock.so

# Configuration site web 
cd /usr/local/etc/apache24/Includes
touch webdav.conf
echo "
LoadModule dav_module libexec/apache24/mod_dav.so
LoadModule auth_digest_module libexec/apache24/mod_auth_digest.so
LoadModule dav_fs_module libexec/apache24/mod_dav_fs.so
LoadModule dav_lock_module libexec/apache24/mod_dav_lock.so
Alias /secure-webdav /usr/local/www/secure-webdav
  DavLockDB \"/usr/local/www/secure-webdav/DavLock\"
  <Directory \"/usr/local/www/secure-webdav\">
        Dav On
        AuthType Digest
        AuthName \"Limited access\"
        AuthUserFile \"/usr/local/etc/apache24/Includes/pwdigest\"
        Require valid-user
        Require ip 192.168.1.0
    </Directory>" > webdav.conf

# Création des utilisateurs digest
#Saisie du mot de passe root definie par l'utilisateur
echo "Le nom d'utilisateur pour l'authentification digest:" 
read username
htdigest -c /usr/local/etc/apache24/Includes/pwdigest "Limited access" $username

# Permissions sur le fichier
 cd /usr/local/etc/apache24/Includes/
 chown www:www pwdigest

# création de la racine webdav
cd /usr/local/www
mkdir secure-webdav
chown www:www secure-webdav

# Redémarrage du serveur web apache
service apache24 restart
