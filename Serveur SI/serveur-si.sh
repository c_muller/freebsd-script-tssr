#!/bin/sh

#Auteur : Corentin MULLER
#Date : 29/11/2023
#Version : 3.0.0 : Met en oeuvre un serveur Apache/MySql avec glpi sur FreeBSD
# Actualisation des versions
# Base php 8.1 (voir version de php contenu dans le paquet glpi)

# Edition du fichier host
echo "glpi  127.0.0.1 
      gestsup 127.0.0.1" >> /etc/hosts

#Installation du service serveur web apache 
pkg install -y apache24

#Service apache démarré au boot 
sysrc apache24_enable=YES

#Installation du package glpi dans sa derniere version (glpi-10.0.7,1)
pkg install -y glpi

#Installation du mod_php (interaction php / apache) (package)
pkg install -y mod_php81

#Installation de l'extension iconv PHP
#pkg install php80-iconv-8.0.18 

# Installation de l'extension apcu 
pkg install -y php81-pecl-APCu-5.1.22

# Installation de l'extension php-dom
pkg install -y php81-dom-8.1.24

# Installation de l'extension PDO (gestsup)
pkg install -y php81-pdo_mysql-8.1.24

#Copie du fichier php.ini 
cd /usr/local/etc
cp php.ini-production php.ini

#Creation d'un fichier de configuration pour php
cd /usr/local/etc/apache24/Includes
touch php.conf
echo "
<FilesMatch \"\.php$\">
	SetHandler application/x-httpd-php
</FilesMatch>
<FilesMatch \"\.phps$\">
	SetHandler application/x-httpd-php-source
</FilesMatch>" > php.conf

# Configuration de securite Apache

cd /usr/local/etc/apache24/Includes
touch security.conf
echo "ServerSignature Off
      ServerTokens Prod" > security.conf

#Creation de la configuration pour glpi (vhost glpi sur le port 8080) 
cd /usr/local/etc/apache24/Includes
touch glpi.conf 
echo "
LoadModule rewrite_module libexec/apache24/mod_rewrite.so
Listen 8080
<VirtualHost *:8080>
ServerName glpi
DocumentRoot \"/usr/local/www/glpi/public\"
    <Directory /usr/local/www/glpi/public>
        Require all granted
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^(.*)$ index.php [QSA,L]
    </Directory>
</VirtualHost>" > glpi.conf 

# Permissions racine web (recursif)
chown -R www:www /usr/local/www

#Installation de mysql server 8.0
pkg install -y mysql80-server

#Service mysql server demarre au boot 
sysrc mysql_enable=YES

#Demarrage du service mysql
service mysql-server start 

#Demarrage du service apahce 
service apache24 start

#Assitant de configuration mysql (installation securisee)
mysql_secure_installation

#Redemarrage du service mysql
service mysql-server restart

#Saisie du mot de passe root definie par l'utilisateur
echo "Mot de passe root définie dans mysql_secure_installation" 
read $Password


#Creation de la base de donnees glpi
mysql -uroot -p$Password -e "CREATE DATABASE glpi;"
mysql -uroot -p$Password -e "CREATE USER 'glpi-user'@'localhost' IDENTIFIED BY 'glpi-user';"
mysql -uroot -p$Password -e "GRANT ALL ON glpi.* TO 'glpi-user'@'localhost';"
mysql -uroot -p$Password -e "FLUSH PRIVILEGES;"

#Creation de la base de donnees gestsup
mysql -uroot -p$Password -e "CREATE DATABASE gestsup;"
mysql -uroot -p$Password -e "CREATE USER 'gestsup-user'@'localhost' IDENTIFIED BY 'gestsup-user';"
mysql -uroot -p$Password -e "GRANT ALL ON gestsup.* TO 'gestsup-user'@'localhost';"
mysql -uroot -p$Password -e "FLUSH PRIVILEGES;"

# Creation du repertoire gestsup a la racine web
cd /usr/local/www
mkdir gestsup

#Creation de la configuration pour gestsup (Alias /gestsup) 
cd /usr/local/etc/apache24/Includes
touch gestsup.conf
echo "Alias /gestsup /usr/local/www/gestsup
      <Directory /usr/local/www/gestsup>
          Options -Indexes
          DirectoryIndex index.php
          Require all granted
      </Directory>" > gestsup.conf

# Creation tache CRON - sauvegarde sql / glpi (a 12h45 chaque vendredi)
echo "45 12 * * 5 mysqldump -u root -p$Password  glpi > /root/backup_sql/glpi.sql
      45 12 * * 5 mysqldump -u root -p$Password  gestsup > /root/backup_sql/gestsup.sql
      * * * * * /usr/local/bin/php /usr/local/www/glpi/front/cron.php" >> /etc/crontab

cd /root 
mkdir backup_sql
#Message post installation 
echo "
1- Base de donnees MySql pour glpi
    - Utilisateur : glpi-user
    - Mot de passe : glpi-user
    - Nom de la base de donnees : glpi

2- Base de donnees MySql pour gestsup
    - Utilisateur : gestsup-user
    - Mot de passe : gestsup-user
    - Nom de la base de donnees : gestsup


3- Definir la timezone pour php dans le fichier /usr/local/etc/php.ini (date.timezone = Europe/Paris)
4- Ajouter index.php dans la configuration Apache /usr/local/etc/apache24/httpd.conf
 
<IfModule dir_module> 
	DirectoryIndex index.html index.php
</IfModule>

Autorisation pour le répertoire gestsup (/usr/local/www/gestsup)
5- chown -R www:www /usr/local/www/gestsup

6- Redemarrer apache : service apache24 restart

7- Mot de passe admin par défaut pour l'application gestsup : admin/admin
"