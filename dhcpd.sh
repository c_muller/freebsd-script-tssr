#!/bin/sh

#Auteur : Corentin MULLER
#Date : 22/12/2019
#Version : 1.0.0 : Met en oeuvre un serveur DHCP sur FreeBSD avec interaction

#Installation du serveur DHCP : iscdhcp server 4.4 (package)
pkg install isc-dhcp44-server-4.4.1_4
sleep 10
#Configuration du service dhcp
cd /usr/local/etc 
sleep 2
#Suppression du fichier de configuration existant (si existant)
rm dhcpd.conf 
sleep 2

#Configuration du dhcp a modifier cas echeant
touch dhcpd.conf 
sleep 2
echo "max-lease-time 7200 ;
ddns-update-style none ;
authoritative ;
log-facility local7 ;
subnet 192.168.14.0 netmask 255.255.255.0 {
range 192.168.14.1 192.168.14.10 ;
}
" > /usr/local/etc/dhcpd.conf
sleep 2
sysrc dhcpd_enable=YES
sleep 2

# Mise en place des logs 
cd /var/log
mkdir dhcpd
cd dhcpd/
touch dhcpd.log
sleep 2
echo "!dhcpd" >> /etc/syslog.conf
echo "*.*  /var/log/dhcpd/dhcpd.log" >> /etc/syslog.conf
sleep 2

# Demarrage du service dhcp
/usr/local/etc/rc.d/isc-dhcpd start
sleep 10