#!/bin/sh

#Auteur : Corentin MULLER
#Date : 09/12/2022
#Version : 2.0.1 : Met en oeuvre un serveur Apache/MySql avec glpi sur FreeBSD
# Actualisation des versions
# Base php 8.0 (voir version de php contenu dans le paquet glpi)

#Installation du service serveur web apache 
pkg install -y apache24

#Service apache démarré au boot 
sysrc apache24_enable=YES

#Installation du package glpi dans sa derniere version (glpi-9.5.7,1)
pkg install -y glpi

#Installation du mod_php (interaction php / apache) (package)
pkg install -y mod_php80

#Installation de l'extension iconv PHP
#pkg install php80-iconv-8.0.18 

# Installation de l'extension apcu 
pkg install -y php80-pecl-APCu-5.1.21

# Installation de l'extension php-dom
pkg install -y php80-dom-8.0.25

#Copie du fichier php.ini 
cd /usr/local/etc
cp php.ini-production php.ini

#Creation d'un fichier de configuration pour php
cd /usr/local/etc/apache24/Includes
touch php.conf
echo "
<FilesMatch \"\.php$\">
	SetHandler application/x-httpd-php
</FilesMatch>
<FilesMatch \"\.phps$\">
	SetHandler application/x-httpd-php-source
</FilesMatch>" > php.conf

#Creation de la configuration pour glpi (Alias Web) 
cd /usr/local/etc/apache24/Includes
touch glpi.conf 
echo " Alias /glpi /usr/local/www/glpi
    <Directory /usr/local/www/glpi>
        AllowOverride Options FileInfo Limit
        Options Indexes FollowSymLinks
        Require all granted
        DirectoryIndex index.php
    </Directory>" > glpi.conf 

# Permissions racine web (recursif)
chown -R www:www /usr/local/www

#Installation de mysql server 8.0
pkg install -y mysql80-server

#Service mysql server demarre au boot 
sysrc mysql_enable=YES

#Demarrage du service mysql
service mysql-server start 

#Demarrage du service apahce 
service apache24 start

#Assitant de configuration mysql (installation securisee)
mysql_secure_installation

#Redemarrage du service mysql
service mysql-server restart

#Saisie du mot de passe root definie par l'utilisateur
echo "Mot de passe root définie dans mysql_secure_installation" 
read $Password

#Creation de la base de donnees glpi
mysql -uroot -p$Password -e "CREATE DATABASE glpi;"
mysql -uroot -p$Password -e "CREATE USER 'glpi-user'@'localhost' IDENTIFIED BY 'glpi-user';"
mysql -uroot -p$Password -e "GRANT ALL ON glpi.* TO 'glpi-user'@'localhost';"
mysql -uroot -p$Password -e "FLUSH PRIVILEGES;"

#Message post installation 
echo "
- Base de donnees MySql pour glpi
    - Utilisateur : glpi-user
    - Mot de passe : glpi-user 
    - Nom de la base de donnees : glpi

- Definir la timezone pour php dans le fichier /usr/local/etc/php.ini (date.timezone = Europe/Paris)
- Ajouter index.php dans la configuration Apache /usr/local/etc/apache24/httpd.conf
 
<IfModule dir_module> 
	DirectoryIndex index.html index.php
</IfModule>
Redemarrer apache : service apache24 restart"





