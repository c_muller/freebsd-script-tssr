Import-Module ActiveDirectory

# Mise en service AD LAB.
$DATAList = Import-Csv "C:\Users\Administrateur\Desktop\utilisateurs-groupes.csv" -Delimiter ";" -Encoding UTF8

# VARIABLES (DEBUG)

# Utilisateur / Groupe
#echo $DATAList.type
# Utilisateur / Groupe
#echo $DATAList.nom
# Utilisateur / Groupe 
#echo $DATAList.nom_affichage
# Utilisateur
#echo $DATAList.user_surname
# Utilisateur 
#echo $DATAList.SamAccountName
# Groupe
#echo $DATAList.groupe_scope
# Groupe
#echo $DATAList.groupe_category
# Groupe
#echo $DATAList.Description
# Utilisateur 
#echo $DATAList.UserPrincipalName
# Utilisateur
#echo $DATAList.Path
# Utilisateur
#echo $DATAList.user_password
# Utilisateur
#echo $DATAList.enabled

#Création des OU
New-ADOrganizationalUnit -Name "Utilisateurs" -Path "DC=lab,DC=net" -ProtectedFromAccidentalDeletion $False
New-ADOrganizationalUnit -Name "Groupes" -Path "DC=lab,DC=net" -ProtectedFromAccidentalDeletion $False
New-ADOrganizationalUnit -Name "Ordinateurs" -Path "DC=lab,DC=net" -ProtectedFromAccidentalDeletion $False

ForEach ($data in $DATAList) 
{


 if ($data.type -eq 'utilisateur') {
   # Pour un utilisateur


   New-ADUser -Name $data.nom_affichage -GivenName $data.nom -Surname $data.user_surname  -SamAccountName $data.SamAccountName -UserPrincipalName $data.UserPrincipalName -Path $data.Path -AccountPassword (ConvertTo-SecureString $data.user_password -AsPlainText -Force) -Enabled ([System.Convert]::ToBoolean($data.enabled))

   Write-Host "l'utilisateur $($data.nom_affichage) a été ajouté correctement : $?"
   
 } else {
  # Pour un groupe
   New-ADGroup -Name $data.nom -DisplayName $data.nom_affichage -SamAccountName $data.nom -GroupScope $data.groupe_scope -GroupCategory $data.groupe_category -Path $data.Path -Description $data.Description
   
    Write-Host "le groupe $($data.nom_affichage) a été ajouté correctement : $?"
   

 }
}
