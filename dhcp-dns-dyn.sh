#!/bin/sh

#Auteur : Corentin MULLER
#Date : 11/09/2022
#Version : 2.0 : Met en oeuvre un serveur DHCP et son DNS dynamique sur FreeBSD avec interaction

# Edition du fichier hosts
echo "
10.14.0.254    BSD14
10.14.0.254    BSD14.DOM14.TSSR.
127.0.0.1      BSD14
127.0.0.1      BSD14.DOM14.TSSR.
" >> /etc/hosts

# Changement du nom d'hôte
hostname BSD14.DOM14.TSSR.

#Installation du serveur DHCP : iscdhcp server 4.4 (package)
pkg install -y isc-dhcp44-server-4.4.2P1_1 
sleep 10
#Configuration du service dhcp
cd /usr/local/etc 
sleep 2
#Copie du fichier de configuration existant (sauvegarde)
cp dhcpd.conf dhcpd_old.conf
sleep 2

#Configuration du dhcp a modifier cas echeant
touch dhcpd.conf 
sleep 2
echo "max-lease-time 7200 ;
Include \"/usr/local/etc/namedb/rndc.key\";
ddns-update-style interim ;
authoritative ;
option domain-name \"DOM14.TSSR.\";
authoritative;
log-facility local7 ;
subnet 10.14.0.0 netmask 255.255.0.0 {
range 10.14.0.1 10.14.0.10 ;
option domain-name-servers 10.14.0.254;
}
zone DOM14.TSSR. {
primary 127.0.0.1;
key rndc-key;
}
zone 14.10.in-addr.arpa. {
primary 10.14.0.254;
key rndc-key;
}
" > /usr/local/etc/dhcpd.conf
sleep 2
sysrc dhcpd_enable=YES
sleep 2

# Mise en place des logs 
cd /var/log
mkdir dhcpd
cd dhcpd/
touch dhcpd.log
sleep 2
echo "!dhcpd" >> /etc/syslog.conf
echo "*.*  /var/log/dhcpd/dhcpd.log" >> /etc/syslog.conf
sleep 2
#Installation et configuration du serveur dns 

#Installation du serveur DNS (package) : bind9
pkg install -y bind918-9.18.9 
sleep 10
#Edition du fichier resolv.conf
echo "search DOM14.TSSR." > /etc/resolv.conf
echo "nameserver 127.0.0.1" >> /etc/resolv.conf 
sleep 2
sysrc named_enable=YES
echo "named_flags=\"-4\"" >> /etc/rc.conf 
sleep 2
echo "Options {
Directory \"/usr/local/etc/namedb/dynamic\" ;
Pid-file \"/var/run/named/pid\" ;
Dump-file \"/var/dump/named_dump.db\" ;
Statistics-file \"/var/stats/named.stats\" ;
listen-on { 127.0.0.1 ; 10.14.0.254 ;} ;
forwarders {8.8.8.8;};
} ;
Include \"/usr/local/etc/namedb/rndc.key\" ;
Zone \"DOM14.TSSR.\" {
Type master ;
File \"direct14\" ;
Allow-update { key rndc-key ; 127.0.0.1 ; 10.14.0.254 ;} ;
} ;
Zone \"14.10.in-addr.arpa.\" {
Type master ;
File \"10.rev\" ;
Allow-update { key rndc-key ; 10.14.0.254 ;} ;
} ;
" > /usr/local/etc/namedb/named.conf
sleep 2

#Creation des fichiers de zone (directe / inversee)
cd /usr/local/etc/namedb/dynamic
touch direct14
touch 10.rev
chown bind:bind direct14
chown bind:bind 10.rev
sleep 2
#Edition du fichier de zone directe

echo "\$ORIGIN .
\$TTL 10800 ;
DOM14.TSSR IN SOA BSD14.DOM14.TSSR. adm.dom14.tssr.(
			1 ;
			3600 ;
			600 ;
			604800 ;
			86400 ;
			)
		NS BSD14.DOM14.TSSR.
\$ORIGIN DOM14.TSSR.
BSD14 A 10.14.0.254" > direct14

#Edition du fichier de zone reverse 
echo "\$ORIGIN .
\$TTL 3600 ;
14.10.in-addr.arpa IN SOA BSD14.DOM14.TSSR. adm.dom14.tssr. (
			1 ;
			3600 ;
			600 ;
			604800 ;
			86400 ;
			)
		NS BSD14.DOM14.TSSR.
\$ORIGIN 14.10.in-addr.arpa.
254.0 PTR BSD14.DOM14.TSSR." > 10.rev
sleep 2
# Generation de la cle de cryptage rndc utilisee par bind et dhcpd 
rndc-confgen -a
sleep 2
# Demarrage des services
/usr/local/etc/rc.d/isc-dhcpd start
sleep 10
service named start
sleep 10
