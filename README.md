# FreeBSD Script TSSR

**Script de mise en oeuvre de services serveur sur FreeBSD**


**Script :  dhcp-dns-dyn.sh** 

Action : Permets la mise en oeuvre d'un serveur DHCP, d'un serveur DNS dynamique : chaque client recevant une adresse de ce serveur voit son nom d'hôte enregistré automatiquement dans la base DNS.

Configuration du script par défaut : 
- Plage d'adresse du serveur DHCP : 10.14.0.1-10.14.0.10 (masque de sous réseau : 255.255.0.0 /16)
- Nom du domaine DNS : DOM14.TSSR.
- Adresse IP du serveur DNS / DHCP : 10.14.0.254

Exécution du script (en tant qu'utilisateur root)
Rendre le script exécutable avec la commande : chmod +x dhcp-dns-dyn.sh 
Exécuter le script : ./dhcp-dns-dyn.sh

Poste configuration à l'issue de l'exécution du script
bsdconfig 
 - /Network Managment/ Network interfaces : Définir une adresse IP fixe (10.14.0.254)
 - /hostname/domain : Définir le nom d'hôte (BSD14) 


**Script : dhcpd.sh**

Action : Permet la mise en oeuvre d'un serveur DHCP 

Configuration du script par défaut : 
- Plage d'adresse du serveur DHCP : 192.168.14.1-192.168.14.10 (masque de sous réseau : 255.255.255.0 /24)
- Adresse IP du serveur DHCP : 192.168.14.254

Post configuration à l'issue de l'exécution du script
bsdconfig 
 - /Network Managment/ Network interfaces : Définir une adresse IP fixe (192.168.14.254)
 
 
**Script : web-server.sh (version 2)**

Action : Permet la mise en oeuvre d'un serveur apache (2.4), serveur de base de données mysql (8.0), et de php (8.1) - Lab développement web

**Script :  create-vhost.sh**

Action : Permet la mise en oeuvre d'un serveur virtuel sur le serveur web Apache (serveur virtuel)

**Script :  glpi-v2.sh**

Action : Permet l'installation de l'application web GLPI (Gestion Libre du Parc Informatique) sur base php 8.0 et mysql 8.0

**Me contacter**
[c-muller.ovh]: https://www.c-muller.ovh/about.html
