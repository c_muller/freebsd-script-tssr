#!/bin/sh

#Auteur : Corentin MULLER
#Date : 12/01/2025
#Version : 1.0 : Met en oeuvre un serveur DHCP Kea sur FreeBSD

# Liste des packages
# bind920-9.20.4
# kea-2.6.1_2

# Adresse machine : 192.168.14.201
# Commande de test de la configuration :  kea-dhcp4 -t /usr/local/etc/kea/kea-dhcp4.conf


# Installation des packages

pkg update

pkg install -y kea-2.6.1_2


# Activation de kea dhcp4 server au boot
sysrc kea_enable=YES


# Parametrage de la configuration
mv /usr/local/etc/kea/kea-dhcp4.conf /usr/local/etc/kea/kea-dhcp4.conf.bkp

# Creation du repertoir de log pour kea dhcp server
cd /var/
mkdir kea-dhcp-server

# Creation du fichier csv de base de donnée 
cd /var/kea-dhcp-server/
touch kea-leases4.csv

# Affichage ifconfig
ifconfig

# Saisie utilisateur - nom de l'interface
echo "Entrez le nom de l'interface reseau (ex : em0) : "
read interface


# Edition de la configuration
echo "
{
    \"Dhcp4\": {
        \"interfaces-config\": {
            \"interfaces\": [
                \"$interface\"
            ]
        },
        \"valid-lifetime\": 691200,
        \"renew-timer\": 345600,
        \"rebind-timer\": 604800,
        \"authoritative\": true,
        \"lease-database\": {
            \"type\": \"memfile\",
            \"persist\": true,
            \"name\": \"/var/kea-dhcp-server/kea-leases4.csv\",
            \"lfc-interval\": 3600
        },
        \"subnet4\": [
            {
            	\"id\": 1,
                \"subnet\": \"192.168.14.0/24\",
                \"pools\": [
                    {
                        \"pool\": \"192.168.14.100 - 192.168.14.120\"
                    }
                ],
                \"option-data\": [
                    {
                        \"name\": \"domain-name-servers\",
                        \"data\": \"192.168.14.201\"
                    },
                    {
                        \"name\": \"domain-search\",
                        \"data\": \"lab.local\"
                    },
                    {
                        \"name\": \"routers\",
                        \"data\": \"192.168.14.2\"
                    }
                ]
            }
        ]
    }
}
" > /usr/local/etc/kea/kea-dhcp4.conf

service kea restart