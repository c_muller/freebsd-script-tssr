#!/bin/bash
# Auteur : Corentin MULLER
# Date : 05/05/2022
# Description : Recuperation des temperatures peripheriques et envoi sur une base influxdb
# Execution : crontab */5 **** (toute les 5 minutes)
# Recuperation des temperatures - composant
tempACPI=$(sensors acpitz-acpi-0 | awk -F '[-+°]' '/temp1:/ {printf("%d\n",$2)}')
tempNvme=$(sensors nvme-pci-0400 | awk -F '[-+°]' '/Composite:/ {printf("%d\n",$2)}')
tempCPU=$(sensors coretemp-isa-0000 | grep "Package id 0:" | awk -F '[-+°]' '/Package id 0:/ {printf("%d\n",$2)}')

curl -i -XPOST -u <user>:<password> "http://<influxdb server @ip / hostname>:8086/write?db=<your db>" --data-raw "acpi_temp,host=<your host>,region=<your location> value=$tempACPI"

curl -i -XPOST -u <user>:<password> "http://<influxdb server @ip / hostname>:8086/write?db=<your db>" --data-raw "nvme_temp,host=<your host>,region=<your location> value=$tempNvme"

curl -i -XPOST -u <user>:<password> "http://<influxdb server @ip / hostname>:8086/write?db=<your db>" --data-raw "cpu_temp,host=<your host>,region=<your location> value=$tempCPU"
