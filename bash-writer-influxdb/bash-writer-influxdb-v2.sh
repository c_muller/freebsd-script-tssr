#!/bin/bash
# Auteur : Corentin MULLER
# Date : 05/05/2022
# Description : Recuperation des temperatures peripheriques et envoi sur une base influxdb
# Execution : crontab */5 **** (toute les 5 minutes)
# Recuperation des temperatures - composant
tempACPI=$(sensors acpitz-acpi-0 | awk -F '[-+°]' '/temp1:/ {printf("%d\n",$2)}')
tempNvme=$(sensors nvme-pci-0400 | awk -F '[-+°]' '/Composite:/ {printf("%d\n",$2)}')
tempCPU=$(sensors coretemp-isa-0000 | grep "Package id 0:" | awk -F '[-+°]' '/Package id 0:/ {printf("%d\n",$2)}')
tempHDD=$(/usr/sbin/hddtemp /dev/sda | awk '{print $NF+0}')

curl --request POST \
"http://10.0.31.184:8086/api/v2/write?org=YOUR ORGANIZATION&bucket=YOUR BUKET&precision=ns" \
  --header "Authorization: Token YOUR TOKEN" \
  --header "Content-Type: text/plain; charset=utf-8" \
  --header "Accept: application/json" \
  --data-binary '
    tempSensors,sensor_id=acpi_temp temperature='"$tempACPI"'
    tempSensors,sensor_id=nvme_temp temperature='"$tempNvme"'
    tempSensors,sensor_id=cpu_temp temperature='"$tempCPU"'
    tempSensors,sensor_id=hdd_temp temperature='"$tempHDD"'
    '