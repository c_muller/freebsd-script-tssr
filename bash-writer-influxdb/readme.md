# Script de récupération de la température des composants et envoi des donnés dans influxdb
## Prérequis 
0. Avoir un serveur avec influxdb et Grafana installé et configuré, vous trouverez les informations nécessaires dans cet article : [Monitoring Grafana](https://c_muller.frama.io/mod19/grafana-monitoring.html)
1. Sur le serveur proxmox, installer le package **lm-sensors** pour afficher les températures des composants. 
`apt install lm-sensors` 
2. Détecter les différents capteurs de températures
`sensors-detect`
3. Vérifier le fonctionnement de la commande **sensors**

---
root@pve:~# sensors
coretemp-isa-0000
Adapter: ISA adapter
Package id 0:  +39.0°C  (high = +100.0°C, crit = +100.0°C)
Core 0:        +39.0°C  (high = +100.0°C, crit = +100.0°C)
Core 1:        +39.0°C  (high = +100.0°C, crit = +100.0°C)
Core 2:        +39.0°C  (high = +100.0°C, crit = +100.0°C)
Core 3:        +39.0°C  (high = +100.0°C, crit = +100.0°C)

pch_cannonlake-virtual-0
Adapter: Virtual device
temp1:        +34.0°C

acpitz-acpi-0
Adapter: ACPI interface
temp1:        +44.0°C  (crit = +119.0°C)

iwlwifi_1-virtual-0
Adapter: Virtual device
temp1:            N/A

nvme-pci-0400
Adapter: PCI adapter
Composite:    +48.9°C  (low  =  -0.1°C, high = +84.8°C)
                       (crit = +94.8°C)

---

## Mise en place du script

1. Télécharger le fichier get-temp.sh présent sur le repo framagit sur proxmox. 
2. Adapter le fichier en fonction des capteurs donnés par la command **sensors** et de l'adresse de votre serveur influxdb
3. Tester l'execution du script (deux à trois fois) :  si votre configuration est correcte, les messages suivant devraient s'afficher dans le terminale. 

![](https://framagit.org/c_muller/freebsd-script-tssr/-/raw/master/images/script-execution.PNG)

4. Créer une tache cron : executée toute les 5 minutes (exemple d'execution) pour envoyer les différentes métrique sur influxdb.
`crontab -e`
`*/5 * * * * /root/get-temp.sh`

5. Vérifier la remontée des métriques dans Grafana. 
   1. Dans Grafana se rendre dans la rubrique **Explore**, sélectionner la base de donnés où son stockées les métriques 
   ![](https://framagit.org/c_muller/freebsd-script-tssr/-/raw/master/images/grafana-explore-acpi_temp.PNG) 
6. Créer vos graphique à partir des requêtes effectuées depuis la rubrique **Explore**
Voici un exemple de graphique
![](https://framagit.org/c_muller/freebsd-script-tssr/-/raw/master/images/grafana_dashboard.PNG)
   

7. Documentation inflxdb 

[Writing_data guide](https://archive.docs.influxdata.com/influxdb/v1.2/guides/writing_data/) 
